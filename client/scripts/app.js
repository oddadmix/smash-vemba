jQuery(document).ready(function () {

    /**
     * Dropdown event for change
     */
    jQuery(genreDropdown).change(function () {
        setLocalGenre(getCurrentGenre().text);
        loadNextVideo();
    });
    /**
     * Event for the video button
     */
    jQuery(nextVideoButton).click(function () {
        nextVideo();
    });
    /**
     * Pagination event
     */
    jQuery(document).on('click', '.page', function (e) {
        var pageNumber = jQuery(e.currentTarget).data('page');
        loadViewed(pageNumber);
    });


    //Load All Genres, Entry point.
    loadGenres();

});
