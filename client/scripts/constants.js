/**
 * Constants for all elements that will be manupilated
 */
const genreDropdown = jQuery('#genre-dropdown select');
const nextVideoButton = jQuery("#next-video");
const videoIdText = jQuery("#video-id");
const videoGenreText = jQuery("#video-genre");
const videoPlayer = jQuery("#video-player");
const prevViewedOl = jQuery("#previously-viewed ol");
const pagination = jQuery("#pagination ul");