function getCurrentUser() {
    var user = localStorage.getItem('user');
    if (user == null) {
        user = uuidv4();
        localStorage.setItem('user', user);
    }
    return user;
}