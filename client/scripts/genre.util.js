function setGenre(genre) {
    jQuery(genreDropdown).text(genre);
    setLocalGenre(genre);
}

function setLocalGenre(genre) {
    localStorage.setItem('genre', genre);
}

function getLocalGenre() {
    return localStorage.getItem('genre');
}

function setGenreText() {
    jQuery(videoGenreText).html(getCurrentGenre().text);
}

function getCurrentGenre() {
    return {
        val: jQuery(genreDropdown).find(":selected").val(),
        text: jQuery(genreDropdown).find(":selected").text()
    }
}

function loadGenres() {
    jQuery.ajax({
        dataType: 'json',
        url: config.baseurl + '/api/genre',
        success: function (data) {
            jQuery(genreDropdown).html('');
            for (var i = 0; i < data.length; i++) {
                if (getLocalGenre() == data[i].name) {
                    jQuery(genreDropdown).append(`<option selected="true" value="${data[i]._id}">${data[i].name}</option>`);
                } else {
                    jQuery(genreDropdown).append(`<option value="${data[i]._id}">${data[i].name}</option>`);
                }

            }
            loadNextVideo();
        }
    });
}