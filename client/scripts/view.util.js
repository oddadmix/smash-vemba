function addView() {
    jQuery.ajax({
        method: 'POST',
        dataType: 'json',
        url: config.baseurl + '/api/view',
        data: {
            videoId: geCurrenttVideo()._id,
            genreId: getCurrentGenre().val,
            userId: getCurrentUser()
        },
        success: function (data) {
            loadNextVideo();
        }
    });
}

function loadViewed(page = 1) {
    const url = config.baseurl + '/api/video/viewed?genreId=' + getCurrentGenre().val + '&userId=' + getCurrentUser() + '&page=' + page;
    jQuery.ajax({
        dataType: 'json',
        url: url,
        success: function (data) {
            jQuery(prevViewedOl).html('');

            for (var i = 0; i < data.videos.length; i++) {
                jQuery(prevViewedOl).append(
                    ` <li class="viewed-video">
                    <div class="video-player-thumbnail" style="background-color:${getColor('' + data.videos[i].pId + data.videos[i].pId)}"></div>
                    <div class="video-viewed-details">
                        <b>Video ${data.videos[i].pId} (${getCurrentGenre().text})</b>
                        <br/>
                        <small>Viewed ${moment(data.videos[i].viewedAt).format('l LTS')}</small>
                    </div>
                </li>`
                );
            }
            loadPagination(data.max_page);
        }
    });
}

/**
 * Create the pagination based on the pages.
 * @param {*} pages 
 */
function loadPagination(pages) {
    jQuery(pagination).html('');
    for (var i = 1; i <= pages; i++) {
        jQuery(pagination).append(`<li class="page" data-page="${i}">${i}</li>`);
    }
}