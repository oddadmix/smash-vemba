function nextVideo() {
    addView();
}

function geCurrenttVideo() {
    return {
        _id: jQuery(videoIdText).attr('data-id'),
        pId: jQuery(videoIdText).attr('data-pid')
    }
}

function setVideo(video) {
    jQuery(videoIdText).html('Video ' + video.pId);
    jQuery(videoIdText).attr('data-id', video._id);
    jQuery(videoIdText).attr('data-pid', video.pId);
}



function disableVideoButton() {
    jQuery(nextVideoButton).html('No more videos in genre');
    jQuery(nextVideoButton).attr('disabled', 'disabled');
}

function enableVideoButton() {
    jQuery(nextVideoButton).html('Next New Video');
    jQuery(nextVideoButton).removeAttr('disabled');

}

function setVideoPlayerColor(pId) {
    document.getElementById("video-player").style.background = getColor("" + pId + pId);
    //jQuery(videoPlayer).css('background', getColor(pId + pId));
}

function loadNextVideo() {
    setGenreText();
    jQuery.ajax({
        dataType: 'json',
        url: config.baseurl + '/api/video/random?genreId=' + getCurrentGenre().val + '&userId=' + getCurrentUser(),
        success: function (response) {
            if (response.next == false) {
                disableVideoButton();
            } else {
                enableVideoButton();
                setVideo(response.video);
                setVideoPlayerColor(response.video.pId);
            }
            loadViewed();
        }
    });
}