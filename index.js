const config = require('./server/config');
const routes = require('./server/routes/index');

const bodyParser = require('body-parser');
const express = require('express');
const mongoose = require('mongoose');
const app = express();

//Setup client folder as the base url to serve index.html
app.use(express.static('client'));

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())


//Setup for the api endpoints
app.get('/api/video/random', routes.getRandomVideo);
app.get('/api/genre', routes.getAllGenres);
app.post('/api/view', routes.addView);
app.get('/api/video/viewed', routes.getViewedVideos);



//Starting the app by listening to the configured port
app.listen(config.port, async function () {
    console.log('Video Smash server started on port ' + config.port);

    //Connecting the the mongodb using the config url. The connection happenes only once.
    mongoose.Promise = global.Promise;
    await mongoose.connect(config.mongo_db_url, { useMongoClient: true });
    console.log('Connected to mongodb');
})