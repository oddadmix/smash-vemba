const config = require('./server/config');
const mongoose = require('mongoose');
const VideoModel = require('./server/models/video');
const GenreModel = require('./server/models/genre');
const ViewsModel = require('./server/models/views');




const geners = ["Action", "Adventure", "Animation", "Biography", "Comedy", "Crime", "Documentary", "Drama", "Family", "Fantasy", "Film-Noir", "Game-Show", "History", "Horror", "Music", "Musical", "Mystery", "News", "Reality-TV", "Romance", "Sci-Fi", "Sport", "Talk-Show", "Thriller", "War", "Western"];
const maxVideos = 10000;

async function runReset() {

    mongoose.Promise = require('bluebird');
    await mongoose.connect(config.mongo_db_url, { useMongoClient: true });


    console.log('Started the db reset');


    console.log('Removing all views');
    await ViewsModel.remove({});

    console.log('Removing all videos');
    await VideoModel.remove({});

    console.log('Removing all generes');
    await GenreModel.remove({});


    for (var genreCounter = 0; genreCounter < geners.length; genreCounter++) {
        console.log('Creating videos for genre: ' + geners[genreCounter]);
        var genre = new GenreModel({ name: geners[genreCounter] });
        try {
            await genre.save();
        } catch (e) {
            console.log(e.toString());
        }

        var videos = [];
        for (var videoCounter = 0; videoCounter < maxVideos; videoCounter++) {
            videos.push(new VideoModel({
                pId: videoCounter + 1,
                genreId: genre._id.toString()
            }));
        }
        await VideoModel.insertMany(videos);
    }

    console.log('Ended the db reset');

    process.exit(0);

}


try {
    runReset();
} catch (e) {
    console.log(e);
}


