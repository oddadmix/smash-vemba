const VideoModel = require('../models/video');
const ViewsModel = require('../models/views');


module.exports = {
    getRandomVideoByGenreIdAndUserId: async function (genreId, userId) {
        console.log(genreId + "/" + userId);

        console.log(views);
        var views = await this.getViewsByUserAndGenre(genreId, userId);
        views = views.map(function (view) { return view.videoId; });
        var counter = await VideoModel.find({ _id: { $nin: views }, genreId: genreId }).count();
        console.log(counter);
        const video = await VideoModel.findOne({ _id: { $nin: views }, genreId: genreId }).skip(Math.floor(Math.random() * counter));
        return video;
    },
    getViewedVideosByUserAndGenre: async function (userId, genreId, pageNumber = 1) {
        var paginate = 10;
        const maxCount = await ViewsModel.find({ userId: userId, genreId: genreId }, {
            __v: false,
            _id: false, genreId: false,
            userId: false
        }).count();
        console.log((pageNumber - 1) * paginate);
        console.log(paginate);
        const views = await ViewsModel.find({ userId: userId, genreId: genreId }, {
            __v: false,
            _id: false, genreId: false,
            userId: false
        }).sort('-viewedAt').skip((pageNumber - 1) * paginate).limit(paginate);
        return { videos: views, size: paginate, page: pageNumber, max_page: Math.ceil(maxCount / paginate) };
    },
    getViewsByUserAndGenre: async function (genreId, userId) {
        var views = await ViewsModel.find({ userId: userId, genreId: genreId }, {
            __v: false,
            _id: false, genreId: false,
            userId: false
        });
        return views;
    }
}