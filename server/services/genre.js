const GenreModel = require('../models/genre');

module.exports = {
    getAllGenres: async function () {
        return await GenreModel.find({});
    }
}