const ViewsModel = require('../models/views');
const VideoModel = require('../models/video');

module.exports = {
    videoViewedByUser: async function (videoId, userId, genreId) {
        const video = await VideoModel.findOne({ _id: videoId });
        const oldView = await ViewsModel.findOne({ videoId: videoId, userId: userId, genreId: genreId });
        if (oldView == null) {
            const view = new ViewsModel({ userId, videoId, genreId, viewedAt: new Date(), pId: video.pId });
            return await view.save();
        }
        return oldView;
    }
}