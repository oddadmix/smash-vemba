const mongoose = require('mongoose');

const model = mongoose.model('Genre', { name: String });

module.exports = model;