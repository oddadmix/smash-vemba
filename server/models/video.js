const mongoose = require('mongoose');

const model = mongoose.model('Video', { pId: Number, genreId: String });

module.exports = model;