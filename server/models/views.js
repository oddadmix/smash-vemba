const mongoose = require('mongoose');

const model = mongoose.model('Views', { userId: String, videoId: String, genreId: String, viewedAt: Date, pId: String });

module.exports = model;