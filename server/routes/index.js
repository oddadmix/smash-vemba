const ViewService = require('../services/views');
const VideoService = require('../services/video');
const GenreService = require('../services/genre');


module.exports = {
    addView: async function (request, response) {
        try {
            console.log(request.body);
            await ViewService.videoViewedByUser(request.body.videoId, request.body.userId, request.body.genreId);
            response.status(200).json({ success: true });
        } catch (e) {
            console.log(e);
            response.status(400).json({ error: e })
        }
    },
    getAllGenres: async function (request, response) {
        try {
            const genres = await GenreService.getAllGenres();
            response.status(200).json(genres);
        } catch (e) {
            console.log(e);
            response.status(400).json({ error: e });
        }
    },
    getRandomVideo: async function (request, response) {
        try {
            const video = await VideoService.getRandomVideoByGenreIdAndUserId(request.query.genreId, request.query.userId);
            console.log(video);
            if (video) {
                response.status(200).json({ next: true, video })
            } else {
                response.status(200).json({ next: false });
            }
        } catch (e) {
            console.log(e);
            response.status(400).json({ error: e });
        }
    },
    getViewedVideos: async function (request, response) {
        try {
            const videos = await VideoService.getViewedVideosByUserAndGenre(request.query.userId, request.query.genreId, request.query.page);
            response.status(200).json(videos);
        } catch (e) {
            console.log(e);
            response.status(400).json({ error: e });
        }
    }
}